/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Cristian
 */
public class personController extends HttpServlet {
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
            ArrayList<String> lstNombres= new ArrayList();
            lstNombres.add("María");
            lstNombres.add("Juanita");
            lstNombres.add("Jorge");
            lstNombres.add("Pepito");
            
            String strName= request.getParameter("name");
            System.out.println("Name capturado: "+strName+" "+lstNombres.get(0));

            
            String strMiDeporte= strName+" juega Básquet"; 
            String strJson= new Gson().toJson(lstNombres);
            JsonObject objJson = new JsonObject();            
            objJson.addProperty("deporte", strJson);
            objJson.addProperty("resultado", true);
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(strJson,1,3);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
